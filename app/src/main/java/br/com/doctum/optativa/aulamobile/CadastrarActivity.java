package br.com.doctum.optativa.aulamobile;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CadastrarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);

        Button button = (Button) findViewById(R.id.botaoCad);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.newUser);
                String nomeUser = editText.getText().toString();

                Usuario usuario = new Usuario();
                usuario.setNome(nomeUser);

                UsuarioDB usuarioDB = new UsuarioDB(getContext());
                usuarioDB.save(usuario);
            }
        });
    }

    public Context getContext() {
        return this;
    }

}
