package br.com.doctum.optativa.aulamobile;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ListarActivity extends AppCompatActivity {
    private static final String TAG = "services";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);

        //this.getEstados(this);
        //this.getEstadosServer(this);
        Button button = (Button) findViewById(R.id.buttonCep);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.campoCep);
                getAdressByCep(getApplicationContext(), editText.getText().toString());
            }
        });

    }

    public static List<Estado> getEstados(Context context) {
        try {
            String json = readFile(context);
            List<Estado> estados = parserJSON(context, json);
            return estados;
        } catch (Exception e) {
            Log.i(TAG, "Erro ao ler os estados" + e.getMessage(), e);
            return null;
        }
    }

    public static List<Estado> getEstadosServer(Context context) {
        try {
            String json = new GetEstadosTask().execute().get();
            List<Estado> estados = parserJSON(context, json);
            return estados;
        } catch (Exception e) {
            Log.i(TAG, "Erro ao ler os estados" + e.getMessage(), e);
            return null;
        }
    }

    private static String loadJsonAssets(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("estados_brasileiros.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private static String readFile(Context context) throws IOException {
        return loadJsonAssets(context);
    }

    public void getAdressByCep(Context context, String cep) {
        try {
            String json = new CepCorreioTask(cep).execute().get();
            CepCorreio cepCorreio = parserJSONCep(context, json);
            TextView textView = (TextView) findViewById(R.id.endereco);
            textView.setText(cepCorreio.getLocalidade() + "- "+ cepCorreio.getLogradouro());
        } catch (Exception e) {
            Log.i(TAG, "Erro ao ler o endereco " + e.getMessage(), e);
        }
    }

    private static List<Estado> parserJSON(Context context, String json) throws IOException {
        List<Estado> estados = new ArrayList<Estado>();

        try {
            JSONObject root = new JSONObject(json);
            JSONObject obj = root.getJSONObject("estados");
            JSONArray jsonEstados = obj.getJSONArray("estado");

            for (int i=0;i<jsonEstados.length();i++) {
                Estado e = new Estado();
                e.setNome(jsonEstados.getJSONObject(i).getString("nome"));
                e.setSigla(jsonEstados.getJSONObject(i).getString("sigla"));
                estados.add(e);
                Log.i(TAG, e.getSigla() + " - " + e.getNome());
            }
            Log.i(TAG, estados.size() + " encontrados");


        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return estados;
    }

    private static CepCorreio parserJSONCep(Context context, String json) throws IOException {
        CepCorreio cepCorreio = new CepCorreio();

        try {
            JSONObject root = new JSONObject(json);
            cepCorreio.setBairro(root.getString("bairro"));
            cepCorreio.setLogradouro(root.getString("logradouro"));
            cepCorreio.setLocalidade(root.getString("localidade"));

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return cepCorreio;
    }
}
