package br.com.doctum.optativa.aulamobile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends LifeCycle {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkbox);

        final EditText editTextUser = (EditText) findViewById(R.id.campoUsuario);
        editTextUser.setText(PreferenciasUser.getValuesString(getContext(),"nameUser"));

        if(!PreferenciasUser.getValuesBoolean(this.getContext(),"materConectado")) {
            Button button = (Button) findViewById(R.id.buttonEntrar);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String user = editTextUser.getText().toString();

                    EditText editTextSenha = (EditText) findViewById(R.id.campoSenha);
                    String senha = editTextSenha.getText().toString();

                    if (user.equals("teste") && senha.equals("123456")) {
                        Intent intent = new Intent(getContext(), DashboardActivity.class);
                        Bundle params = new Bundle();
                        params.putString("nome", user);
                        intent.putExtras(params);

                        PreferenciasUser.setValuesString(getContext(), "nameUser", user);

                        if (checkBox.isChecked()) {
                            PreferenciasUser.setValuesBoolean(getContext(), "materConectado", true);
                        } else {
                            PreferenciasUser.setValuesBoolean(getContext(), "materConectado", false);
                        }

                        startActivity(intent);
                    } else {
                        alert("Falha ao efetuar login");
                    }

                }
            });
        }
        else {
            Intent intent = new Intent(getContext(), DashboardActivity.class);
            startActivity(intent);
        }
    }

    public void alert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    public Context getContext() {
        return this;
    }

}
