package br.com.doctum.optativa.aulamobile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class UsuarioDB extends SQLiteOpenHelper {
    //nome do banco
    public static final String NOME_BANCO = "projeto_aula";
    private static final int VERSAO_BANCO = 1;
    private static final String TAG = "sql";

    public UsuarioDB(Context context){
        //context, nome do banco, factory, versao
        super(context,NOME_BANCO,null,VERSAO_BANCO);
    }
    @Override
    public void onCreate(SQLiteDatabase db){
        Log.d(TAG, "Criando a tabela Usuario...");
        db.execSQL("create table if not exists usuario(_id integer primary key autoincrement," +
                "usuario text,senha text,nome text,email text,telefone text,idade text);");
        Log.d(TAG, "Tabela Usuario criada com sucesso");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
        //Caso mude a versao do banco de dados, podemos executar um SQL aqui
    }
    //Insere um novo usuario, ou atualiza se ja existe
    public long save(Usuario usuario){
        long id = usuario.getId();
        SQLiteDatabase db = getWritableDatabase();

        try{
            ContentValues values = new ContentValues();
            values.put("usuario",usuario.getUsuario());
            values.put("senha",usuario.getSenha());
            values.put("nome",usuario.getNome());
            values.put("email",usuario.getEmail());
            values.put("telefone",usuario.getTelefone());
            values.put("idade", usuario.getIdade());
            if(id!=0){
                String _id = String.valueOf(usuario.getId());
                String[] whereArgs = new String[]{_id};
                //update usuario set values= ... where _id=?
                int count = db.update("usuario",values,"_id=?",whereArgs);
                return count;
            }
            else{
                //insert into usuario values (...)
                id = db.insert("usuario","",values);
                return id;
            }
        }finally {
            db.close();
        }
    }
    //delete o usuario
    public int delete(Usuario usuario){
        SQLiteDatabase db = getWritableDatabase();
        try{
            //delete from usuario where _id=?
            int count = db.delete("usuario", "_id=?", new String[]{String.valueOf(usuario.id)});
            Log.i(TAG, "Deletou [" + count + "] registro");
            return count;
        }
        finally {
            db.close();
        }
    }
    //deleta o usuario pelo nome
    public int deleteUsuarioByNome(String user){
        SQLiteDatabase db = getWritableDatabase();
        try{
            //delete from usuario where _id=?
            int count = db.delete("usuario", "usuario=?", new String[]{user});
            Log.i(TAG, "Deletou [" + count + "] registro");
            return count;
        }
        finally {
            db.close();
        }
    }
    //Consulta a lista com todos os usuarios
    public List<Usuario> findAll(){
        SQLiteDatabase db = getWritableDatabase();
        try{
            //select *from usuario
            Cursor c = db.query("usuario", null, null, null, null, null, null, null);
            return toList(c);
        }
        finally {
            db.close();
        }
    }
    //Consulta o usuario pelo nome
    public List<Usuario> findAllByTipo(String user){
        SQLiteDatabase db = getWritableDatabase();
        try{
            //select *from usuario where nome=?
            Cursor c = db.query("usuario", null, "usuario='" + user + "'", null, null, null, null, null);
            return toList(c);
        }
        finally {
            db.close();
        }
    }
    //Lê o cursor e cria a lista de usuarios
    private List<Usuario> toList(Cursor c){
        List<Usuario> user = new ArrayList<Usuario>();

        if(c.moveToFirst()){
            do{
                Usuario u = new Usuario();
                user.add(u);
                //recupera os atributos de usuario
                u.id = c.getInt(c.getColumnIndex("_id"));
                u.usuario = c.getString(c.getColumnIndex("usuario"));
                u.senha = c.getString(c.getColumnIndex("senha"));
                u.nome = c.getString(c.getColumnIndex("nome"));
                u.email = c.getString(c.getColumnIndex("email"));
                u.telefone = c.getString(c.getColumnIndex("telefone"));
                u.idade = c.getString(c.getColumnIndex("idade"));


            }while(c.moveToNext());
        }
        return user;
    }
    //Executa um SQL
    public void execSQL(String sql){
        SQLiteDatabase db = getWritableDatabase();
        try{
            db.execSQL(sql);
        }
        finally {
            db.close();
        }
    }
    //Executa um SQL
    public void execSQL(String sql,Object[] args){
        SQLiteDatabase db = getWritableDatabase();
        try{
            db.execSQL(sql,args);
        }
        finally {
            db.close();
        }
    }
}
